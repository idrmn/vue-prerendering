const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const src = path.resolve(__dirname, '../src')
const scss = path.resolve(__dirname, '../src/assets/scss/')
const nodeModules = path.resolve('node_modules/')

exports.assetsPath = function (_path) {
  return path.posix.join('static', _path)
}

exports.cssLoaders = (options) => {
  options = options || {}
  
  const cssLoader = {
    loader: 'css-loader',
    options: {
      url: false,
      minimize: process.env.NODE_ENV === 'production',
      sourceMap: options.sourceMap
    }
  }
  
  const generateLoaders = (loader, loaderOptions) => {
    const loaders = [cssLoader]
    if (loader) {
      loaders.push({
        loader: loader + '-loader',
        options: Object.assign({}, loaderOptions, {
          sourceMap: options.sourceMap
        })
      })
    }
    
    if (options.extract) {
      return ExtractTextPlugin.extract({
        use: loaders,
        fallback: 'vue-style-loader'
      })
    } else {
      return ['vue-style-loader'].concat(loaders)
    }
  }
  
  return {
    css: generateLoaders(),
    postcss: generateLoaders(),
    sass: generateLoaders('sass', {indentedSyntax: true, includePaths: [src, scss, nodeModules]}),
    scss: generateLoaders('sass', {includePaths: [src, scss, nodeModules]})
  }
}

exports.styleLoaders = function (options) {
  const output = []
  const loaders = exports.cssLoaders(options)
  for (let extension in loaders) {
    const loader = loaders[extension]
    output.push({
      test: new RegExp('\\.' + extension + '$'),
      use: loader
    })
  }
  return output
}
