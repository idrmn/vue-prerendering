module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  globals: {
    ENV: true,
    require: true
  },
  env: {
    browser: true,
    es6: true
  },
  extends: 'standard',
  plugins: [
    'html'
  ],
  rules: {
    'no-new': 0,
    'one-var': 0,
    'no-unused-vars': [
      'error',
      {
        'vars': 'local',
        'args': 'none',
        'ignoreRestSiblings': true
      }],
    'arrow-parens': 0,
    'no-console': 0,
    'brace-style': ['error', '1tbs'],
    'generator-star-spacing': 0,
    'indent': ['error', 2, {'VariableDeclarator': {'var': 2, 'let': 2, 'const': 2}}],
    'quotes': [
      'error',
      'single', {'allowTemplateLiterals': true}
    ],
    'comma-dangle': [
      'error',
      'only-multiline'
    ],
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
}
