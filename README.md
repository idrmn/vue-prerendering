# README #

A simple Webpack + vue-loader + prerender-spa-plugin setup for quick prototyping.

### What is this repository for? ###

Prerenders static HTML in a single-page application

### Usage ###

``` bash
$ git clone https://idrmn@bitbucket.org/idrmn/vue-prerendering.git
$ cd vue-prerendering
$ npm install 
$ npm run dev
```
### Production ready build ###

``` bash
% npm run build
```