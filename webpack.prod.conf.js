require('./build/check-versions')()

const path = require('path')
const utils = require('./build/utils')
const webpack = require('webpack')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./build/webpack.base.conf')
const PrerenderSpaPlugin = require('prerender-spa-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const routerOptions = require('./build/config')

const DIST = path.resolve(__dirname, './dist')

var webpackConfig = merge(baseWebpackConfig, {
  module: {
    rules: utils.styleLoaders({
      sourceMap: true,
      extract: true
    })
  },
  devtool: '#source-map',
  output: {
    path: DIST,
    publicPath: '/',
    filename: '[name].build.js'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': '"production"'
    }),
    new CleanWebpackPlugin([DIST], {
      root: __dirname,
      verbose: true,
      dry: false,
      exclude: []
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      sourceMap: true
    }),
    new ExtractTextPlugin({
      filename: utils.assetsPath('css/[name].[contenthash].css')
    }),
    new OptimizeCSSPlugin({
      cssProcessorOptions: {
        safe: true
      }
    }),
    new HtmlWebpackPlugin({
      title: 'Vue Prerender',
      template: './index.html',
      filename: path.resolve(__dirname, 'dist/index.html'),
      inject: 'head',
      chunksSortMode: 'dependency'
    }),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, './static'),
        to: 'static',
        ignore: ['.*']
      }
    ]),
    new CompressionWebpackPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: new RegExp(
          '\\.(' +
          ['js', 'css'].join('|') +
          ')$'
      ),
      threshold: 10240,
      minRatio: 0.8
    }),
    new PrerenderSpaPlugin(DIST, routerOptions.map(router => router.path),{
      captureAfterElementExists: '#app',
      captureAfterTime: 1000,
    })
  ]
})

module.exports = webpackConfig
