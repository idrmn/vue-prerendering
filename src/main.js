import Vue from 'vue'
import smoothScroll from 'smoothscroll-polyfill'
import VueCurrencyFilter from 'vue-currency-filter'
import Vue2Filters from 'vue2-filters'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'

import './assets/scss/app.scss'

Vue.use(Vuelidate)
Vue.use(Vue2Filters)
Vue.use(VueCurrencyFilter, {
  symbol: 'PLN',
  thousandsSeparator: ' ',
  fractionCount: 2,
  fractionSeparator: '.',
  symbolPosition: 'back',
  symbolSpacing: true
})

smoothScroll.polyfill()

document.addEventListener('DOMContentLoaded', function () {
  const root = new Vue({
    el: '#app',
    router,
    render: h => h(App)
  })
  root.$mount('#app')
})
