import Vue from 'vue'
import VueRouter from 'vue-router'
import routerOptions from '../build/config'

Vue.use(VueRouter)

const routes = routerOptions.map(route => {
  return {
    path: route.path,
    component: () => import(/* webpackChunkName: "js/[request]" */`@/pages/${route.component}`),
    meta: route.meta
  }
})

const router = new VueRouter({
  mode: 'history',
  routes: [
    ...routes
  ]
})

export default router
